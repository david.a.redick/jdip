A fork of [jdip](http://jdip.sourceforge.net) that runs on modern java.


To Build:

```
ant build
```

To Run:
```
ant run
```
